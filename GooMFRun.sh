#!/bin/bash
echo '==================================================='
if [ $# -ne 3 ]; then 
        echo 'usage: ./GooMFRun "path to properties file" "path to application dir" "path to GooMF"'    
        exit 0
fi

PROP_FILE_PATH=$1
APPLICATION_PATH=$2
GOOMF_PATH=$3
if [ ${PROP_FILE_PATH:0:1} = "." ]; then
        PROP_FILE_PATH=`pwd`${PROP_FILE_PATH:1:${#PROP_FILE_PATH}}
else
        PROP_FILE_PATH=`pwd`$PROP_FILE_PATH
fi
if [ ${APPLICATION_PATH:0:1} = "." ]; then
        APPLICATION_PATH=`pwd`${APPLICATION_PATH:1:${#PROP_FILE_PATH}}
else
        APPLICATION_PATH=`pwd`$APPLICATION_PATH
fi
if [ ${GOOMF_PATH:0:1} = "." ]; then
        GOOMF_PATH=`pwd`${GOOMF_PATH:1:${#PROP_FILE_PATH}}
else
        GOOMF_PATH=`pwd`$GOOMF_PATH
fi

echo 'properties file path = '$PROP_FILE_PATH
echo 'application path = '$APPLICATION_PATH
echo 'GooMF library path = '$GOOMF_PATH

echo '==================================================='
echo 'generating program state and kernels...'
cd $GOOMF_PATH
cd ./GooMFGenerator
./GooMFGenerator -c $PROP_FILE_PATH -o $APPLICATION_PATH -f ifs

echo '==================================================='
echo 'copying generated files...'
cp ./GooMF_CPU_monitor.h $GOOMF_PATH'/GooMFLibrary/GooMF_CPU_monitor.h'
cp ./ProgramState.h $APPLICATION_PATH'/ProgramState.h'
cp ./GooMF_GPU_monitor_alg_finite.cl $APPLICATION_PATH'/GooMF_GPU_monitor_alg_finite.cl'
cp ./GooMF_GPU_monitor_alg_infinite.cl $APPLICATION_PATH'/GooMF_GPU_monitor_alg_infinite.cl'
cp ./GooMF_GPU_monitor_alg_partial.cl $APPLICATION_PATH'/GooMF_GPU_monitor_alg_partial.cl'
echo $GOOMF_PATH'/GooMFLibrary/GooMF_CPU_monitor.h'
echo $APPLICATION_PATH'/ProgramState.h'
echo $APPLICATION_PATH'/GooMF_GPU_monitor_alg_finite.cl'
echo $APPLICATION_PATH'/GooMF_GPU_monitor_alg_infinite.cl'
echo $APPLICATION_PATH'/GooMF_GPU_monitor_alg_partial.cl'
echo '==================================================='
echo 'recompiling GooMF shared library...'
cd $GOOMF_PATH/GooMFLibrary
echo 'currently in:'
pwd
echo 'compiling...'
gcc -I/opt/AMDAPP/include -fPIC -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"GooMF.d" -MT"GooMF.d" -o"GooMF.o" "GooMF.c"
gcc -I/opt/AMDAPP/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"ParamRepo.d" -MT"ParamRepo.d" -o"ParamRepo.o" "ParamRepo.c"
echo 'linking...'
gcc -L/opt/AMDAPP/lib -shared -o"libGooMF.so"  ./GooMF.o ./ParamRepo.o   -lOpenCL -lm
cp libGooMF.so /usr/lib/libGooMF.so
ldconfig -n /usr/lib
echo 'ldconfig...'
echo ''

echo "finished!"
exit 0

