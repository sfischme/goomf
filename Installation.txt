To install and run GooMF, follow the following steps:

1) Download the source code to local directory GOOMF_PATH

2) Run sudo ./GooMFMake.sh from GOOMF_PATH

3) Instrument the inspected program with the calls to GooMF API (see GOOMF_PATH/Examples)

4) Run sudo ./GooMFRun.sh "path to properties file" "path to application dir" "path to GooMF"
	from GOOMF_PATH