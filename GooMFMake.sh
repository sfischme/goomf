#!/bin/bash
echo '==================================================='
echo 'Compiling GooMFGenerator...'
#pwd
cd ./GooMFGenerator
#pwd
gcc -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"src/MonitorGenerator.d" -MT"src/MonitorGenerator.d" -o"src/MonitorGenerator.o" "src/MonitorGenerator.c"
echo 'Linking GooMFGenerator...'
gcc -L/usr/local/lib -L/home/shay/libconfig-1.4.8/lib -o"GooMFGenerator"  ./src/MonitorGenerator.o   -lconfig -lm
echo '==================================================='

echo "finished!"
exit 0
