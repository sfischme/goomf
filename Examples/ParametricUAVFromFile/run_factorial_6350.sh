#!/bin/bash

# factors declaration
algorithms_array=( 0 1 2 )
num_of_properties_array=( 1 2 3 )
buffer_size_array=( 1 256 512 1024 2048 4096 8192 )
destinations_array=( 0 12 24 )
#output file header
echo -e "Run\tAlgorithm\tProperties\tBufferSize\tDestinations\tDevice\tTime" >> out.txt

for algorithms_index in 0 1 2
do

 for properties_index in 0 1 2
 do

  for buffer_size_index in 0 1 2 3 4 5 6
  do

	for destinations_index in 0 1 2
	do

  	echo "./Release/a.out -n ${num_of_properties_array[properties_index]} -b ${buffer_size_array[buffer_size_index]} -a ${algorithms_array[algorithms_index]} -c 1 -f /home/rtesg/goomf/Examples/ParametricUAV/iceberg_flight_data_positions_${destinations_array[destinations_index]}destinations.txt -o /home/rtesg/goomf/Examples/ParametricUAVFromFile/out.txt"

	i="0"
	while [ $i -lt 100 ]
	do
		echo -ne "$i\t${algorithms_array[algorithms_index]}\t${num_of_properties_array[properties_index]}\t${buffer_size_array[buffer_size_index]}\t${destinations_array[destinations_index]}\tHD6350\t" >> out.txt	

		nice -n -10 ./Release/a.out -n ${num_of_properties_array[properties_index]} -b ${buffer_size_array[buffer_size_index]} -a ${algorithms_array[algorithms_index]} -c 1 -f /home/rtesg/goomf/Examples/ParametricUAV/iceberg_flight_data_positions_${destinations_array[destinations_index]}destinations.txt -o /home/rtesg/goomf/Examples/ParametricUAVFromFile/out.txt
		
		i=$[$i+1]
	done
    	done
   done 
  done
done

echo "finished!"
exit 0
