/*
 ============================================================================
 Name        : ParametricUAV.c
 Author      : Shay Berkovich
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "math.h"
#include <time.h>
#include <sched.h>
#include <GooMFOfflineAPI.h>
#include "../ProgramState.h"
#include "../globals.h"
#include "../UDPLayer.h"

extern int sched_setaffinity(pid_t pid, size_t cpusetsize, cpu_set_t *mask);
extern int pthread_create(pthread_t *thread, const pthread_attr_t *attr,
	    void *(*start_routine)(void*), void *arg);
extern int pthread_join(pthread_t thread, void **retval);

_GOOMF_context context = NULL;
struct _GOOMF_host_program_state_struct data[ITERATIONS];
struct timespec ts1, ts2;
_GOOMF_enum_verdict_type v[3];
_GOOMF_enum_alg_type _GLOBAL_algorithm;

/*
 * Measures difference between 2 timestamps
 */
struct timespec diff(struct timespec start, struct timespec end)
{
	struct timespec temp;
	if ((end.tv_nsec - start.tv_nsec)<0) {
		temp.tv_sec = end.tv_sec - start.tv_sec - 1;
		temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
	} else {
		temp.tv_sec = end.tv_sec - start.tv_sec;
		temp.tv_nsec = end.tv_nsec - start.tv_nsec;
	}
	return temp;
}


/*
 * Printing the results
 */
void printResults()
{
	clock_gettime(CLOCK_REALTIME, &ts2);
	struct timespec ts3 = diff(ts1, ts2);

	if (_GLOBAL_output_file == NULL)
	{
		char str0[100];
		char str1[100];
		char str2[100];
		_GOOMF_getCurrentStatus(context, v);
		_GOOMF_typeToString(&v[0], str0);
		_GOOMF_typeToString(&v[1], str1);
		_GOOMF_typeToString(&v[2], str2);
		printf("%ld sec, %ld microsec, %ld nanosec;\nproperty 0: %s;\nproperty 1: %s;\nproperty2: %s;\n",
			ts3.tv_sec,
			ts3.tv_nsec / 1000,
			ts3.tv_nsec,
			str0, str1, str2);
	}
	else
	{
		FILE* fout = fopen(_GLOBAL_output_file, "a");
		if (fout != NULL)
			fprintf(fout, "%ld\n", ts3.tv_sec * 1000 + ts3.tv_nsec / 1000000);
		fclose(fout);
	}
}

/*
 * Fill the data array from the input file
 */
int fillData()
{
	FILE* f = NULL;
	char tempLine[MAX_LINE_LENGTH];

	if ((f = fopen(_GLOBAL_input_file, "r")) == NULL)
		return -1;

	fgets(tempLine, MAX_LINE_LENGTH, f);	//to skip titles
	while (fscanf(f, "%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n",
			&(data[_GLOBAL_data_counter].x),
			&(data[_GLOBAL_data_counter].y),
			&(data[_GLOBAL_data_counter].llh_lat),
			&(data[_GLOBAL_data_counter].llh_lon),
			&(data[_GLOBAL_data_counter].llh_alt),
			&(data[_GLOBAL_data_counter].gps_second),
			&(data[_GLOBAL_data_counter].llh_lat_dest),
			&(data[_GLOBAL_data_counter].llh_lon_dest),
			&(data[_GLOBAL_data_counter].llh_alt_dest),
			&(data[_GLOBAL_data_counter].llh_lat_dest_prev),
			&(data[_GLOBAL_data_counter].llh_lon_dest_prev),
			&(data[_GLOBAL_data_counter].llh_alt_dest_prev)) != EOF)
	{
		++_GLOBAL_data_counter;
	}

	fclose(f);
	return 0;
}

int workerFunction()
{
	int i = 0, Result = _GOOMF_SUCCESS;

//int j = 0;
//for (j=0; j<10; ++j)
//{
//	//start timer only after first buffer is flushed (warm-up period)
//	if (i == _GLOBAL_buffer_size)
//		clock_gettime(CLOCK_REALTIME, &ts1);

	for (i=0; i<_GLOBAL_data_counter; ++i)
	{
		//start timer only after first buffer is flushed (warm-up period)
		if (i == _GLOBAL_buffer_size)
			clock_gettime(CLOCK_REALTIME, &ts1);

		Result = _GOOMF_nextState(context, (void*)(data + i));
		if (Result != _GOOMF_SUCCESS)
		{
			char str[100];
			_GOOMF_getErrorDescription(Result, str);
			printf("%s", str);
			return EXIT_FAILURE;
		}
	}
	_GOOMF_flush(context);

//}

	return EXIT_SUCCESS;
}

/*
wrap function
*/
void* wrapFunction(void* ptr)
{
	unsigned long mask = 4;	//setting the specific core

	int result = sched_setaffinity(0, sizeof(mask), &mask);
	if (result < 0)
	{
		printf("Cannot set affinity to CPU thread.\n");
		return NULL;
	}

	//affinity is set - now do the work
	workerFunction();
	return NULL;
}


/*
Run sequential execution on one CPU core
*/
void initAndRunCPUsingleCore()
{
	pthread_t cpu_thread;

	int result = pthread_create(&cpu_thread, NULL, wrapFunction, NULL);
    if (result != 0)
	{
        printf("Cannot create thread for CPU execution\n");
		return;
	}

	pthread_join(cpu_thread, NULL);
	return;
}


/*
Run sequential execution w/o spawning new thread
*/
void initAndRunCPUmultiCore()
{
	workerFunction();
	return;
}


/*
 * MAIN
 */
int main(int argc, char** argv)
{
	int c, Result = _GOOMF_SUCCESS, alg = 0, temp = 0;

	//parsing the arguments
	while ((c = getopt(argc, argv, "b:f:a:o:c:n:")) != -1)
	{
		switch (c) {
		case 'b':
			_GLOBAL_buffer_size = atoi(optarg);
			break;
		case 'f':
			_GLOBAL_input_file = optarg;
			break;
		case 'a':
			alg = atoi(optarg);
			if (alg == 0) _GLOBAL_algorithm = _GOOMF_enum_alg_seq;
			else if (alg == 1) _GLOBAL_algorithm = _GOOMF_enum_alg_partial_offload;
			else if (alg == 2) _GLOBAL_algorithm = _GOOMF_enum_alg_finite;
			else _GLOBAL_algorithm = _GOOMF_enum_alg_infinite;
			break;
		case 'o':
			_GLOBAL_output_file = optarg;
			break;
		case 'c':
			temp = atoi(optarg);
			if (temp == 1)
				_GLOBAL_multicore = false;
			else
				_GLOBAL_multicore = true;
			break;
		case 'n':
			_GLOBAL_properties_enabled = atoi(optarg);
			break;
		default:
			fprintf(stderr, "Wrong usage.\n");
			return -1;
		}
	}

	if (fillData() < 0)
		return EXIT_FAILURE;

	if ((Result =_GOOMF_initContext(&context,
				_GOOMF_enum_buffer_trigger,
				_GLOBAL_algorithm,
				_GOOMF_enum_sync_invocation,
				_GLOBAL_buffer_size)) != _GOOMF_SUCCESS)
	{
		char str[100];
		_GOOMF_getErrorDescription(Result, str);
		printf("%s", str);
		return EXIT_FAILURE;
	}

	switch (_GLOBAL_properties_enabled)
	{
	case 0:
		_GOOMF_disableProperty(context, 0);
		_GOOMF_disableProperty(context, 1);
		_GOOMF_disableProperty(context, 2);
		break;
	case 1:
		_GOOMF_disableProperty(context, 1);
		_GOOMF_disableProperty(context, 2);
		break;
	case 2:
		_GOOMF_disableProperty(context, 2);
		break;
	default:
		break;
	}

	if (_GLOBAL_algorithm == 0 && _GLOBAL_multicore)
		initAndRunCPUmultiCore();
	else if (_GLOBAL_algorithm == 0 && !_GLOBAL_multicore)
		initAndRunCPUsingleCore();
	else workerFunction();

	printResults(_GLOBAL_output_file);

	Result = _GOOMF_destroyContext(&context);
	if (Result != _GOOMF_SUCCESS)
	{
		char str[100];
		_GOOMF_getErrorDescription(Result, str);
		printf("%s", str);
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
