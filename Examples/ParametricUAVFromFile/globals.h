#ifndef _GLOBALS_
#define _GLOBALS_
#include <stdbool.h>


#define APPLICATION_NAME				"ParametricUAVMonitor"
#define MONITOR_PORT 					51112
#define INPUT_SIZE 						45
#define OUTPUT_SIZE 					4
#define ITERATIONS 						32768
#define UDP_MAX_ROWS 					1000
#define NUM_OF_TIMESTAMP_PACKETS 		15
#define RESOLUTION 						1000
#define MAX_LINE_LENGTH 				10000

//array of controller times in microseconds
bool _GLOBAL_monitoring = false;
bool _GLOBAL_debug = false;
unsigned int _GLOBAL_buffer_size = 1;
char* _GLOBAL_input_file = NULL;
char* _GLOBAL_output_file = NULL;
bool _GLOBAL_multicore = false;
int _GLOBAL_data_counter = 0;
int _GLOBAL_properties_enabled = 3;
#endif /* _GLOBALS_ */

