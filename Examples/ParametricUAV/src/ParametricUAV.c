/*
 ============================================================================
 Name        : ParametricUAV.c
 Author      : Shay Berkovich
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "math.h"
#include <time.h>
#include <GooMFOfflineAPI.h>
#include "../ProgramState.h"
#include "../globals.h"
#include "../UDPLayer.h"

typedef struct
{
	int alive;
	int s;
	int packet_counter;
} user_struct_type;
user_struct_type _GLOBAL_user_context;

extern int getNumOfProperties();

int open_session()
{
	_GLOBAL_user_context.alive = getNumOfProperties();
	_GLOBAL_user_context.packet_counter = 0;
	if (openInConnection(&(_GLOBAL_user_context.s), MONITOR_PORT) < 0)
			return -1;

	return 0;
}

int get_next_state(void* next_program_state_ptr)
{
	//all properties converged
	if (_GLOBAL_user_context.alive <= 0)
		return -1;

	//receive only ITERATIONS packets
	if ((_GLOBAL_user_context.packet_counter)++ >= ITERATIONS)
		return -1;

	//blocking receive
	if (receivePacket(_GLOBAL_user_context.s, (char*)(next_program_state_ptr), sizeof(struct _GOOMF_host_program_state_struct)) < 0)
	{
		closeConnection(_GLOBAL_user_context.s);
		return -1;
	}

	return 0;
}

int close_session()
{
	closeConnection(_GLOBAL_user_context.s);
	return 0;
}

int report(int prop_num, _GOOMF_enum_verdict_type verdict_type, const void* program_state)
{
	printf("Property %d has reached state %d;\n", prop_num, verdict_type);
	/*printf("Program state: %f; %f; %f; %d; %ld\n",
	printf("Program state: %f; %f; %f\n",
			((struct _GOOMF_host_program_state_struct*)program_state)->x,
			((struct _GOOMF_host_program_state_struct*)program_state)->y,
			((struct _GOOMF_host_program_state_struct*)program_state)->z
			((struct _GOOMF_host_program_state_struct*)program_state)->num_of_sats,
			((struct _GOOMF_host_program_state_struct*)program_state)->timestamp
			);*/
	if (--(_GLOBAL_user_context.alive) <= 0)
		printf("All properties converged!\n");

	return 0;
}

int main(int argc, char** argv)
{
	int c, Result = _GOOMF_SUCCESS;

	//parsing the arguments
	while ((c = getopt(argc, argv, "m:")) != -1)
	{
		switch (c) {
		case 'm':
			MONITOR_MODE = true;
			break;
		default:
			fprintf(stderr, "Usage: ./%s -m true/false(monitoring mode)\n", APPLICATION_NAME);
			return -1;
		}
	}

	if (MONITOR_MODE)
	{
		if ((Result = _GOOMF_analyze(open_session,
				get_next_state,
				close_session,
				report,
				_GOOMF_enum_buffer_trigger,
				_GOOMF_enum_alg_finite,
				_GOOMF_enum_sync_invocation,
				stdout,
				256)) != _GOOMF_SUCCESS)
		{
			char error_message[1000];
			_GOOMF_getErrorDescription(Result, error_message);
			printf("%s\n", error_message);
			return EXIT_FAILURE;
		}
	}

	return EXIT_SUCCESS;
}
