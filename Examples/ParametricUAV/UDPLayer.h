/*
 ============================================================================
 Name        : UDPLayer.h
 Author      : Shay Berkovich
 Version     :
 Copyright   : Real-Time Embedded Software Group, University of Waterloo
 Description : Services for UDP setup/send/receive
 ============================================================================
 */
#ifndef UDPLAYER_H_
#define UDPLAYER_H_

#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#define BUFLEN 512
#define NPACK 10

extern void * memset ( void * ptr, int value, size_t num );

/*
 * Setup the receiving connection
 * Returns 0 if everything is OK
 * Returns -1 if socket problems
 * Returns -2 if binding problems
 */
int openInConnection(int* s, unsigned int local_port)
{
	struct sockaddr_in address_me;

	if ((*s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
	    return -1;

	//setting up the local bind
	memset((char*)(&address_me), 0, sizeof(address_me));
	address_me.sin_family = AF_INET;
	address_me.sin_port = htons(local_port);

	address_me.sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind((*s), (struct sockaddr *)&address_me, sizeof(address_me)) != 0)
		return -2;

	return 0;
}


/*
 * Setup the sending connection
 * Returns 0 if everything is OK
 * Returns -1 if socket problems
 * Returns -2 if binding problems
 */
int openOutConnection(int* s)
{
	if ((*s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
	    return -1;

	return 0;
}


/*
 * Send the packet
 */
int sendPacket(int s, char* buf, unsigned int buf_len, char* remote_address, unsigned int remote_port)
{
	struct sockaddr_in address_other;

	memset((char *) &address_other, 0, sizeof(address_other));
	address_other.sin_family = AF_INET;
	address_other.sin_port = htons(remote_port);
	if (inet_aton(remote_address, &address_other.sin_addr) ==0 )
		return -1;

	if (sendto(s, buf, buf_len, 0, (struct sockaddr *)&address_other, sizeof(address_other)) == -1)
		return -2;

	return 0;
}


/*
 * Blocking receive the packet
 * s - socket
 * buf - buffer to store the data
 * buf_len - the length of the buffer in bytes
 */
int receivePacket(int s, char* buf, unsigned int buf_len)
{
	struct sockaddr_in address_from;
	unsigned int from_length = sizeof(address_from);

	if (recvfrom(s, buf, buf_len, 0, &address_from, &from_length) == -1)
	{
		printf( "Error receiving packet: %s\n", strerror( errno ) );
		return -1;
	}

	return 0;
}


/*
 * Close the connection
 * s - socket
 */
int closeConnection(int s)
{
	close(s);
	return 0;
}

#endif /* UDPLAYER_H_ */
