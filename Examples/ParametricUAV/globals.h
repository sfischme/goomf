#ifndef _GLOBALS_
#define _GLOBALS_
#include <stdbool.h>

#define 	TRUE   1
#define 	FALSE  0

#define 	UBUNTU
//#define 	QNX

#define APPLICATION_NAME				"ParametricUAVMonitor"
#define MONITOR_ADDRESS  				"127.0.0.1"
#define MONITOR_PORT 					51112
#define INPUT_SIZE 						45
#define OUTPUT_SIZE 					4
#define ITERATIONS 						1024
#define UDP_MAX_ROWS 					1000
#define NUM_OF_TIMESTAMP_PACKETS 		15
#define RESOLUTION 						1000
#define MAX_LINE_LENGTH 				10000
#define OUTPUT_TEMP_FILE 				"output_temp.txt"
#define OUTPUT_REFERENCE_FILE 			"output_reference.txt"
#define OUTPUT_TIMES_FILE 				"output_controller_times.txt"
#define OUTPUT_VEHICLE_TIMESTAMPS_FILE 	"output_vehicle_times.txt"
#define OUTPUT_STATISTICS_FILE 			"output_statistics.txt"
#define INPUT_FILE						"/home/rtesg/goomf/ParametricUAVFromFile/iceberg_flight_data_positions.csv"

//array of controller times in microseconds
unsigned long times_array[ITERATIONS];
bool MONITOR_MODE = false;
bool DEBUG_MODE = false;
const unsigned int BUFFER_SIZE = 1;

typedef struct {
	double 	y1;
	double 	y2;
	double 	y3;
	double 	y4;
}controller_output_t;

typedef struct {
    double Fz1;
    double Fz2;
    double Fz3;
    double Fz4;

    double Fx1;
    double Fx2;
    double Fx3;
    double Fx4;

    double Fy1;
    double Fy2;
    double Fy3;
    double Fy4;

    double mux1;
    double mux2;
    double mux3;
    double mux4;
    double muy1;
    double muy2;
    double muy3;
    double muy4;

    double Q1_max;
    double Q2_max;
    double Q3_max;
    double Q4_max;

    double Q1_min;
    double Q2_min;
    double Q3_min;
    double Q4_min;


    double delta1;
    double delta2;
    double delta3;
    double delta4;

    double Ex;
    double Ey;
    double Ez;

    double wFx;
    double wFy;
    double wGz;

    double wxd1;
    double wxd2;
    double wxd3;
    double wxd4;

    double gamma;
    double Q_gain;
	double velocity;

    int n;
    int dummy;

}controller_data_t;

typedef struct {
	double timestamps[UDP_MAX_ROWS];
}timestamps_t;

#endif /* _GLOBALS_ */

