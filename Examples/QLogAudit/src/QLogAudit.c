/*
 ============================================================================
 Name        : QLogAudit.c
 Author      : Shay Berkovich
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <GooMFOnlineAPI.h>
#include <GooMFOfflineAPI.h>
#include "../ProgramState.h"

typedef struct
{
	int alive;
	FILE* f;
} user_struct_type;

user_struct_type _GLOBAL_user_context;

/*
 * Callback for _GOOMF_open(void*)
 * Has to return >= 0 if successful or <0 otherwise
 */
int _GOOMF_open()
{
	_GLOBAL_user_context.alive = 4;

	if ((_GLOBAL_user_context.f = fopen("log_example.txt", "r")) == 0)
		return -1;

	return 0;
}
/*
int _GOOMF_open()
{
	_GLOBAL_user_context.alive = 4;

	if ((_GLOBAL_user_context.f = fopen("param_prop_log_example.txt", "r")) == 0)
		return -1;

	return 0;
}*/


/*
 * Callback for _GOOMF_get_next_state(void*).
 * This function has to fill next_program_state.
 * Has to return >= 0 if successful or <0 otherwise
 */
int _GOOMF_get_next_state(void* next_program_state)
{
	//all properties converged
	if (_GLOBAL_user_context.alive <= 0)
		return -1;

	//parsing next line
	float x = 0.0, y = 0.0, z = 0.0;
	unsigned int num_of_sats = 0;
	unsigned long timestamp = 0;
	if (fscanf(_GLOBAL_user_context.f, "%f\t%f\t%f\t%d\%ld\n", &x, &y, &z, &num_of_sats, &timestamp) == EOF)
		return -1;

	((struct _GOOMF_host_program_state_struct*)next_program_state)->x = x;
	((struct _GOOMF_host_program_state_struct*)next_program_state)->y = y;
	((struct _GOOMF_host_program_state_struct*)next_program_state)->z = z;
	((struct _GOOMF_host_program_state_struct*)next_program_state)->num_of_sats = num_of_sats;
	((struct _GOOMF_host_program_state_struct*)next_program_state)->timestamp = timestamp;

	return 0;
}
/*
int _GOOMF_get_next_state(void* next_program_state)
{
	//all properties converged
	if (_GLOBAL_user_context.alive <= 0)
		return -1;

	//parsing next line
	if (fscanf(_GLOBAL_user_context.f, "%d\t%d\t%c\t%f\t%d\t%d\n",
			&(((struct _GOOMF_host_program_state_struct*)next_program_state)->timestamp),
			&(((struct _GOOMF_host_program_state_struct*)next_program_state)->action),
			&(((struct _GOOMF_host_program_state_struct*)next_program_state)->fdesc),
			&(((struct _GOOMF_host_program_state_struct*)next_program_state)->bid),
			&(((struct _GOOMF_host_program_state_struct*)next_program_state)->owner),
			&(((struct _GOOMF_host_program_state_struct*)next_program_state)->num_of_sats)) == EOF)
		return -1;

	return 0;
}*/



/*
int _GOOMF_get_next_state(void* next_program_state)
{
	//all properties converged
	if (_GLOBAL_user_context.alive <= 0)
		return -1;

	float phi1 = 0.0, teta1 = 0.0, ksi1 = 0.0, p1 = 0.0, q1 = 0.0, r1 = 0.0, phi2 = 0.0, teta2 = 0.0, ksi2 = 0.0;
	if (fscanf(_GLOBAL_user_context.f, "%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f",
			&phi1, &teta1, &ksi1, &p1, &q1, &r1, &phi2, &teta2, &ksi2) == EOF)
		return -1;

	((struct _GOOMF_host_program_state_struct*)next_program_state)->phi1 = phi1;
	((struct _GOOMF_host_program_state_struct*)next_program_state)->teta1 = teta1;
	((struct _GOOMF_host_program_state_struct*)next_program_state)->ksi1 = ksi1;
	((struct _GOOMF_host_program_state_struct*)next_program_state)->p1 = p1;
	((struct _GOOMF_host_program_state_struct*)next_program_state)->q1 = q1;
	((struct _GOOMF_host_program_state_struct*)next_program_state)->r1 = r1;
	((struct _GOOMF_host_program_state_struct*)next_program_state)->phi2 = phi2;
	((struct _GOOMF_host_program_state_struct*)next_program_state)->teta2 = teta2;
	((struct _GOOMF_host_program_state_struct*)next_program_state)->ksi2 = ksi2;

	return 0;
}
*/

/*
 * Callback for _GOOMF_close(void*)
 * Has to return >= 0 if successful or <0 otherwise
 */
int _GOOMF_close()
{
	fclose(_GLOBAL_user_context.f);
	printf("All parsed.\n");
	return 0;
}


/*
 * Callback for _GOOMF_report()
 * Has to return >= 0 if successful or <0 otherwise
 */
/*
int _GOOMF_report(int prop_num, _GOOMF_enum_verdict_type verdict_type, const void* program_state)
{
	printf("Property %d has reached state %d;\n", prop_num, verdict_type);
	printf("Program state: %f; %f; %f; %f; %f; %f; %f; %f; %f\n",
			((struct _GOOMF_host_program_state_struct*)program_state)->phi1,
			((struct _GOOMF_host_program_state_struct*)program_state)->teta1,
			((struct _GOOMF_host_program_state_struct*)program_state)->ksi1,
			((struct _GOOMF_host_program_state_struct*)program_state)->p1,
			((struct _GOOMF_host_program_state_struct*)program_state)->q1,
			((struct _GOOMF_host_program_state_struct*)program_state)->r1,
			((struct _GOOMF_host_program_state_struct*)program_state)->phi2,
			((struct _GOOMF_host_program_state_struct*)program_state)->teta2,
			((struct _GOOMF_host_program_state_struct*)program_state)->ksi2);

	if (--(_GLOBAL_user_context.alive) <= 0)
		printf("All properties converged!\n");

	return 0;
}
*/
int _GOOMF_report(int prop_num, _GOOMF_enum_verdict_type verdict_type, const void* program_state)
{
	printf("Property %d has reached state %d;\n", prop_num, verdict_type);
	printf("Program state: %f; %f; %f; %d; %ld\n",
			((struct _GOOMF_host_program_state_struct*)program_state)->x,
			((struct _GOOMF_host_program_state_struct*)program_state)->y,
			((struct _GOOMF_host_program_state_struct*)program_state)->z,
			((struct _GOOMF_host_program_state_struct*)program_state)->num_of_sats,
			((struct _GOOMF_host_program_state_struct*)program_state)->timestamp);

	if (--(_GLOBAL_user_context.alive) <= 0)
		printf("All properties converged!\n");

	return 0;
}
/*
int _GOOMF_report(int prop_num, _GOOMF_enum_verdict_type verdict_type, const void* program_state)
{
	printf("Property %d has reached state %d;\n", prop_num, verdict_type);
	printf("Program state: %d; %d; %c; %f; %d; %d\n",
				((struct _GOOMF_host_program_state_struct*)program_state)->timestamp,
				((struct _GOOMF_host_program_state_struct*)program_state)->action,
				((struct _GOOMF_host_program_state_struct*)program_state)->fdesc,
				((struct _GOOMF_host_program_state_struct*)program_state)->bid,
				((struct _GOOMF_host_program_state_struct*)program_state)->owner,
				((struct _GOOMF_host_program_state_struct*)program_state)->num_of_sats);

	if (--(_GLOBAL_user_context.alive) <= 0)
		printf("All properties converged!\n");

	return 0;
}*/


/*
 * MAIN
 */
int main(void)
{
	if (_GOOMF_analyze(_GOOMF_open,
			_GOOMF_get_next_state,
			_GOOMF_close,
			_GOOMF_report,
			_GOOMF_enum_buffer_trigger,
			_GOOMF_enum_alg_finite,
			_GOOMF_enum_sync_invocation,
			stdout,
			256) == _GOOMF_SUCCESS)
		return EXIT_SUCCESS;
	else
		return EXIT_FAILURE;
}
