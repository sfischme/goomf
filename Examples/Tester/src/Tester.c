/*
 ============================================================================
 Name        : GMTester.c
 Author      : Shay Berkovich
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "../../ParametricUAV/UDPLayer.h"
#include "../../ParametricUAV/globals.h"
#include "../../ParametricUAV/ProgramState.h"

struct _GOOMF_host_program_state_struct buffer[ITERATIONS];

/*
 * Read next program state from file
 */
int readStateSatellites(FILE* file, struct _GOOMF_host_program_state_struct* ps)
{
	if (file == NULL)
		return -1;

	if (fscanf(file, "%d\t", &(ps->timestamp)) == EOF)
		return -1;

	if (fscanf(file, "%d\t", &(ps->num_of_sat)) == EOF)
		return -1;

	if (fscanf(file, "\n") == EOF)
		return -1;

	return 0;
}

/*
 * Read next program state from file
 */
int readXPositions(FILE* file, struct _GOOMF_host_program_state_struct* ps)
{
	if (file == NULL)
		return -1;

	if (fscanf(file, "%d\t", &(ps->timestamp)) == EOF)
		return -1;

	if (fscanf(file, "%d\t", &(ps->num_of_sat)) == EOF)
		return -1;

	if (fscanf(file, "\n") == EOF)
		return -1;

	return 0;
}

int readData()
{
	FILE* f = fopen(INPUT_FILE, "r");
	if(f == NULL)
		return -1;

	//first line is the heading
	char line[MAX_LINE_LENGTH];
	fgets(line, MAX_LINE_LENGTH, f);

	int i = 0;
	for (i=0; i<ITERATIONS; ++i)
	{
		if (readXPositions(f, &(buffer[i])) != 0)
			//readStatePhiControl(f, &(buffer[i]));
			//readStateSatellites(f, &(buffer[i]));
		{
			fclose(f);
			return -1;
		}
	}

	fclose(f);
	return 0;
}

int main(void)
{
	int out_socket = -1, i = 0;
	struct _GOOMF_host_program_state_struct output_buffer;
	memset(&output_buffer, 0, sizeof(output_buffer));

	if (openOutConnection(&out_socket) < 0)
		return EXIT_FAILURE;

	if (readData() != 0)
	{
		printf("Cannot read from the file %s.", INPUT_FILE);
		return EXIT_FAILURE;
	}

	for (i=0; i<ITERATIONS; ++i)
	{
		if (sendPacket(out_socket,
				(char*)(&(buffer[i])),
				sizeof(struct _GOOMF_host_program_state_struct),
				MONITOR_ADDRESS,
				MONITOR_PORT) < 0)
			return EXIT_FAILURE;

		usleep(1000);
	}

	closeConnection(out_socket);

	printf("Tester: all packets sent.\n");
	return EXIT_SUCCESS;
}
